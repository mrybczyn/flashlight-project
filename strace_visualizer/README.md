Flashlight code repository
==========================

Flashlight is allowing to show what a complicated application is doing,
what changes between versions or system configurations. It uses best
Linux tracing and debugging techniques and shows the data that is important.
This is to allow developers to build better and more secure applications
and start losing time on debugging.

Modules:
strace_visualizer: analyses program strace
