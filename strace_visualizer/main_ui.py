# Flashlight debugging tools
# Copyright (C) 2016 Marta Rybczynska
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from ui.DiffWindow import *
from StraceParser import *
from FileActivityCompare import *

if __name__ == "__main__":
    app = QApplication(sys.argv)
    parser1 = StraceParser(sys.argv[1], None, None, 0)
    parser2 = StraceParser(sys.argv[2], None, None, 0)
    parser1.parse_all()
    parser2.parse_all()
    result = FileActivityCompare(parser1.files, parser2.files)
    result.compare()
#    print("Diff")
#    print(sorted(result.diff.keys()))
#    print("Same")
#    print(sorted(result.same.keys()))
    window = DiffWindow(sorted(result.same.keys()), sorted(result.diff.keys()))
    window.show()
    sys.exit(app.exec_())
