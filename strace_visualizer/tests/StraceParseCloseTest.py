# Flashlight debugging tools
# Copyright (C) 2016 Marta Rybczynska
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from StraceParser import StraceParser
from Database import FileProperties
import unittest


class TestOpenMethods(unittest.TestCase):
    def setUp(self):
        self.p = StraceParser()
        stdin = FileProperties()
        stdout = FileProperties()
        stderr = FileProperties()
        stdin['file_type'] = 'stdin'
        stdout['file_type'] = 'stdout'
        stderr['file_type'] = 'stderr'
        self.expected_files = {
            0: stdin,
            1: stdout,
            2: stderr}
        self.expected_errors = {'name': {}}

    def tearDown(self):
        del self.p
        self.p = None
        self.expected_files = None
        self.expected_errors = None

    def testCloses(self):
        self.p.parse_one(
            'open("/lib/x86_64-linux-gnu/libc.so.6", O_RDONLY|O_CLOEXEC) = 3')
        self.p.parse_one('close(3)                                = 0')
        self.assertEqual(self.p.files.get_all(), self.expected_files)
        self.assertEqual(self.p.errors, self.expected_errors)

if __name__ == '__main__':
    unittest.main()
