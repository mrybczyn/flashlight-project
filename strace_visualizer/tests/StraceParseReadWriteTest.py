# Flashlight debugging tools
# Copyright (C) 2016 Marta Rybczynska
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from StraceParser import StraceParser
from Database import FileProperties
import unittest


class TestReadWriteMethods(unittest.TestCase):
    def setUp(self):
        self.p = StraceParser()
        stdin = FileProperties()
        stdout = FileProperties()
        stderr = FileProperties()
        stdin['file_type'] = 'stdin'
        stdout['file_type'] = 'stdout'
        stderr['file_type'] = 'stderr'
        self.expected_files = {
            0: stdin,
            1: stdout,
            2: stderr}
        self.expected_errors = {'name': {}}

    def tearDown(self):
        del self.p
        self.p = None
        self.expected_files = None
        self.expected_errors = None

    def testReadWrite(self):
        self.p.parse_one(
            'open("/lib/x86_64-linux-gnu/libc.so.6", O_RDONLY|O_CLOEXEC) = 3')
        # First read
        self.p.parse_one(
            'read(3, "\177ELF\2\1\1\3\0\0\0\0\0\0\0\0\3\0>\0"..., 832) = 832')
        self.assertEqual(self.p.files._dict[3]['read_increment'], 832)
        self.assertEqual(self.p.files._dict[3]['write_increment'], 0)
        # First write
        self.p.parse_one('write(3, "\x01\x00\x00\x00\x00\x00\x00\x00", 8) = 8')
        self.assertEqual(self.p.files._dict[3]['read_increment'], 832)
        self.assertEqual(self.p.files._dict[3]['write_increment'], 8)
        # One more read (see increment)
        self.p.parse_one('read(3, "\177", 1) = 1')
        self.assertEqual(self.p.files._dict[3]['read_increment'], 833)
        self.assertEqual(self.p.files._dict[3]['write_increment'], 8)
        # One more write (see increment)
        self.p.parse_one('write(3, "\x00", 1) = 1')
        self.assertEqual(self.p.files._dict[3]['read_increment'], 833)
        self.assertEqual(self.p.files._dict[3]['write_increment'], 9)
        self.p.parse_one('close(3)                                = 0')
        self.assertEqual(self.p.files.get_all(), self.expected_files)
        self.assertEqual(self.p.errors, self.expected_errors)

if __name__ == '__main__':
    unittest.main()
