from StraceParser import StraceParser
from Database import FileProperties
import unittest


class TestUnknownMethods(unittest.TestCase):
    def setUp(self):
        self.p = StraceParser()
        stdin = FileProperties()
        stdout = FileProperties()
        stderr = FileProperties()
        stdin['file_type'] = 'stdin'
        stdout['file_type'] = 'stdout'
        stderr['file_type'] = 'stderr'
        self.expected_files = {
            0: stdin,
            1: stdout,
            2: stderr}
        self.expected_errors = {'name': {}}

    def tearDown(self):
        del self.p
        self.p = None
        self.expected_files = None
        self.expected_errors = None

    def testUnknowns(self):
        self.p.parse_one(
            'execve("/bin/echo", ["echo", "1"], [/* 47 vars */]) = 0')
        self.assertEqual(self.p.files.get_all(), self.expected_files)
        self.assertEqual(self.p.errors, self.expected_errors)

    def testSpecialChars(self):
        self.p.parse_one('open("somefile", O_RDONLY|O_CLOEXEC) = 3')
        self.p.parse_one('open("somefile2", O_RDONLY|O_CLOEXEC) = 8')
        self.p.parse_one(
            'read(3, "# Lo\n# (C), Inc.\n#\n# T; and/oru"..., 4096) = 2492')
        self.p.parse_one(
            'read(8, "<?xml=\"1.0\"?>\n<! \"f.dtd\">\n<!\nE"..., 8192) = 8192')


if __name__ == '__main__':
    unittest.main()
