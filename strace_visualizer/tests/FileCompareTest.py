# Flashlight debugging tools
# Copyright (C) 2016 Marta Rybczynska
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from FileActivityCompare import FileActivityCompare
from StraceParser import StraceParser
import unittest
import sys


class TestFileActivityCompare(unittest.TestCase):
    def setUp(self):
        self.parser1 = StraceParser('kcalc.trace', None, None, 0)
        self.parser2 = StraceParser('kcalc2.trace', None, None, 0)
        self.parser1.parse_all()
        self.parser2.parse_all()
        self.result = FileActivityCompare(
            self.parser1.files, self.parser2.files)
        self.result.compare()

    def tearDown(self):
        del self.parser1
        del self.parser2
        del self.result

    def testFileDiff(self):
        sorted_list = sorted(self.result.diff.keys())
        expected_diff = [
            '/dev/urandom', '/usr/lib/x86_64-linux-gnu/libXi.so',
            '/usr/lib/x86_64-linux-gnu/libXi.so.6',
            '/usr/share/icons/oxygen/16x16/actions/document-revert.png',
            '/usr/share/icons/oxygen/22x22/apps/accessories-calculator.png',
            '/usr/share/icons/oxygen/32x32/actions/format-fill-color.png',
            '/usr/share/icons/oxygen/32x32/apps/preferences-desktop-font.png',
            '/usr/share/icons/oxygen/32x32/apps/preferences-kcalc-' +
            'constants.png']
        python_version = sys.version_info.major
        # The two functions actuall do the same in python 2 and 3
        if python_version == 3:
            self.assertCountEqual(sorted_list, expected_diff)
        else:
            self.assertItemsEqual(sorted_list, expected_diff)

if __name__ == '__main__':
    unittest.main()
