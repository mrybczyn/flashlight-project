# Flashlight debugging tools
# Copyright (C) 2016 Marta Rybczynska
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from Database import DatabaseSqlite
from Database import FileProperties
import unittest


class DatabaseTest(unittest.TestCase):
    def setUp(self):
        self.db = DatabaseSqlite(':memory:')
        self.db.create()

    def tearDown(self):
        del self.db

    def testOpens(self):
        self.assertEqual(0, 0)

    def testAddRemoveEvent(self):
        data = FileProperties()
        data['file_type'] = 'test'
        self.db.add_event(1, data, 0)

        self.db.c.execute(
            "select * from files where file_type=:type", {"type": 'test'})
        result = self.db.c.fetchone()
        self.assertNotEqual(result, None)

        self.assertEqual(result[0], 1)
        self.assertEqual(result[1], 0)
        self.assertEqual(result[2], -1)
        self.assertEqual(result[3], 'test')
        self.assertEqual(result[4], None)
        self.assertEqual(result[5], None)

        self.db.remove_event(1, 1)
        self.db.c.execute(
            "select * from files where file_type=:type", {"type": 'test'})
        # still one when we remove, we've just added the end date
        result = self.db.c.fetchone()
        self.assertNotEqual(result, None)
        self.assertEqual(result[0], 1)
        self.assertEqual(result[1], 0)
        self.assertEqual(result[2], 1)  # this should change
        self.assertEqual(result[3], 'test')
        self.assertEqual(result[4], None)
        self.assertEqual(result[5], None)

if __name__ == '__main__':
    unittest.main()
