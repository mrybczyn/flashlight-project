# Flashlight debugging tools
# Copyright (C) 2016 Marta Rybczynska
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from Database import Database
from Database import FileProperties
import unittest


class DatabaseTest(unittest.TestCase):
    def setUp(self):
        self.db = Database()
        self.db.add_backend('file')
        self.db.add_backend('sqlite', ':memory:')

#   def tearDown(self):
#       self.db.close()

    def testOpens(self):
        self.assertEqual(0, 0)

    def testWrongOpen(self):
        with self.assertRaises(NotImplementedError):
            self.db.add_backend('mysql')

    def testAddRemoveEvent(self):
        data = FileProperties()
        data['file_type'] = 'test'
        self.db.add_event(1, data)
        self.db.remove_event(1)

if __name__ == '__main__':
    unittest.main()
