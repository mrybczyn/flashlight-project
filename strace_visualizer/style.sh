#!/bin/bash

export PYTHONPATH="$PYTHONPATH:`pwd`"

pep8 --show-source --show-pep8 *.py tests/*.py ui/*.py

echo "Stats for main"
pep8 --statistics -qq *.py

echo "Stats for tests"
pep8 --statistics -qq tests/*.py

echo "Stats for ui"
pep8 --statistics -qq ui/*.py
