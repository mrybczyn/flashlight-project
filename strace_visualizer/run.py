# Flashlight debugging tools
# Copyright (C) 2016 Marta Rybczynska
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import errno
import os
import subprocess
import sys


def create_dir(directory):
    """ Create directory for output files

    Arguments:
        directory (str): Directory name
    """

    try:
        os.makedirs(directory)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise
        else:
            print "\nThe directory %s exists already!" % directory

if __name__ == '__main__':
    """ Tracer wrapper

    Run the file racer on the output of user command. Run strace with the right
    parameters and output all files to a directory.

    Syntax:
        python run.py directory yourprogram [args]
    """

    argc = len(sys.argv)
    if (argc < 3):
        print "Syntax: python run.py directory yourprogram [args]"
        exit(1)

    create_dir(sys.argv[1])
    stdoutf = open(sys.argv[1]+'/stdout', 'w')
    stderrf = open(sys.argv[1]+'/stderr', 'w')

    command = ['strace', '-x', '-s', '1024', '-o',
               sys.argv[1] + '/strace.trace'] + sys.argv[2:argc]

    p = subprocess.Popen(
        command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    for line in p.stdout:
        stdoutf.write(line)
    for line in p.stderr:
        stderrf.write(line)
    retval = p.wait()
    stdoutf.close()
    stderrf.close()
