# Flashlight debugging tools
# Copyright (C) 2016 Marta Rybczynska
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import sys
import sqlite3


class FileProperties(dict):
    """ File properties class

     This class holds attributes of one file or file-like object
     (socket etc). Default attributes are specified in this class.

     """

    def __init__(self):
        self['file_name'] = None
        self['file_type'] = None
        self['attributes'] = None
        self['mode'] = None
        self['location'] = None
        self['domain'] = None
        self['type'] = None
        self['protocol'] = None
        self['initval'] = None
        self['flags'] = None
        self['parent_socket'] = None
        self['sockaddr'] = None
        self['socklen'] = None
        self['read_increment'] = 0
        self['write_increment'] = 0


class FileEvent(dict):
    """ File releated event

    This class holds one file object event. Available attributes
    are specified in this class.

    """

    def __init__(self):
        self['fd'] = -1
        self['type'] = None
        self['length'] = 0
        self['result'] = 0
        self['read_increment'] = 0
        self['write_increment'] = 0


class DatabaseText:
    """ Database implementation by text files

    This class implements a database using text files for use with the
    Database class. This is a simplified write only implementation,
    search operations won't work.

    Args:
        filename (str): Name of the file to open

    Attributes:
        outf (file): Output file to write the content
    """

    def __init__(self, filename):
        self.outf = open(filename, 'w')

    def __del__(self):
        if self.outf:
            self.outf.close()

    def create(self):
        return

    def add_event(self, id, val, event_id):
        """ Output an add-type event

        Args:
            id (int): File descriptor id
            val (class): Event content
            event_id (int): Unique event id from EventId class

        Returns:
            none
        """
        self.outf.write(
            str(event_id) + ' ADD ' + str(id) + ' ' + str(val) + '\n')

    def remove_event(self, id, event_id):
        """ Output a close-type event

        Args:
            id (int): File descriptor id
            event_id (int): Unique event id from EventId class

        Returns:
            none
        """
        self.outf.write(str(event_id) + ' CLOSE ' + str(id) + '\n')

    def do_event(self, event, event_id):
        """ Output a different type of event (not open/close)

        Args:
            id (int): File descriptor id
            event (class): Event content
            event_id (int): Unique event id from EventId class

        Returns:
            none
        """
        self.outf.write(str(event_id) + ' EVENT ' + str(event) + '\n')


class DatabaseSqlite:
    """ Database implementation by sqlite files

    This class implements a database using Sqlite for use with the
    Database class. This is a full implementation with search and
    modify operations.

    Args:
        filename (str): Name of the database file to open

    Attributes:
        conn: Database connection
        c: Sqlite cursor
    """
    def __init__(self, filename):
        self.conn = sqlite3.connect(filename)

    def __del__(self):
        self.conn.commit()
        self.conn.close()

    def create(self):
        """ Create the database tables

        Create the database tables that allow to store events.
        """

        self.c = self.conn.cursor()
        # Create table for files
        self.c.execute('''CREATE TABLE files (id INTEGER,
            from_timestamp INTEGER, to_timestamp INTEGER, file_type TEXT,
            file_name TEXT, attributes TEXT, mode TEXT, location TEXT,
            domain TEXT, type TEXT, protocol TEXT)''')
        return

    def add_event(self, id, val, event_id):
        """ Output an add-type event

        Add-type event means inserting the value to the database.

        Args:
            id (int): File descriptor id
            val (class): Event content
            event_id (int): Unique event id from EventId class

        Returns:
            none
        """
        self.c.execute('''INSERT INTO files VALUES
            (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''', (
            id, event_id, -1,
            val['file_type'], val['file_name'], val['attributes'],
            val['mode'], val['location'], val['domain'], val['type'],
            val['protocol']))

    def remove_event(self, id, event_id):
        """ Output a close-type event

        Close-type event means modification of an existing entry with the
        validity end of the descriptor.

        Args:
            id (int): File descriptor id
            event_id (int): Unique event id from EventId class

        Returns:
            none
        """
        self.c.execute('''UPDATE files SET to_timestamp = ? WHERE id=?''', (
            event_id, id))


class EventId:
    """ Generate unique event id

    The generator gives incremented identification numbers for the events.

    Attributes:
        id (int): Next non-used id number
    """

    def __init__(self):
        self.id = 0

    def get_next(self):
        """ Get next unique idenfier

        Returns:
            value (int): New identifier
        """

        value = self.id
        self.id += 1
        return value


class Database:
    """ Generic database implementation

    This class implements the generic database implementation. It can support
    different backends depending on the configuration options. All database
    operation pass by this class and it uses the right implementation for the
    backend database type.

    This class can support zero, one or multiple backends. Currently Text and
    Sqlite are supported. One instance of each type is supported.

    Attributes:
        database_list (list): The list of initialized backends
        timestamp (event): event id source
    """

    def __init__(self):
        self.database_list = {}
        self.timestamp = EventId()

    def add_backend(self, backend_type, name='default_filename'):
        """ Add a new backend.

        The new backend is initialized when added. It won't contain
        events that were added before the database is created.

        Attributes:
            backend_type(str): 'file' for file database or 'sqlite' for sqlite
                type of database
            name (str): Database file name

        Returns:
            None if success
            Exception on creation failure
        """

        if (backend_type == 'file'):
            self.database_list[backend_type] = DatabaseText(name)
        elif (backend_type == 'sqlite'):
            self.database_list[backend_type] = DatabaseSqlite(name)
        else:
            raise NotImplementedError(
                'database ' + backend_type + 'not implemented')
        self.database_list[backend_type].create()

    def add_event(self, id, val):
        """ Add an add-type event in the database to all backends

        Attributes:
            id (int): Descriptor id
            val (event): Operation attributes (file name etc)
        Returns:
            event_id: Unique event id
        """

        event_id = self.timestamp.get_next()
        for key in self.database_list:
            self.database_list[key].add_event(id, val, event_id)
        return event_id

    def remove_event(self, id):
        """ Add an close-type event in the database to all backends

        Attributes:
            id (int): Descriptor id
        Returns:
            event_id: Unique event id
        """
        event_id = self.timestamp.get_next()
        for key in self.database_list:
            self.database_list[key].remove_event(id, event_id)
        return event_id

    def do_event(self, event):
        """ Add an event in the database to all backends

        Attributes:
            event (event): Event data
        Returns:
            event_id: Unique event id
        """
        event_id = self.timestamp.get_next()
        for key in self.database_list:
            self.database_list[key].do_event(event, event_id)
        return event_id
