# Flashlight debugging tools
# Copyright (C) 2016 Marta Rybczynska
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import sys
from StraceParser import FileTracer
from StraceParser import StraceParser
from Database import Database


class FileActivityCompare():
    """ Compare file activity from two databases

    Compare file-related activity from two databases and find out
    same and different files accessed.

    This class uses FileTracer and outputs only files (no sockets and
    other types of descriptors).

    Arguments:
        file1 (FileTracer): First file tracer
        file2 (FileTracer): Second file tracer

    Attributes:
        f1 (FileTracer): First file tracer
        f2 (FileTracer): Second file tracer
        same (list): Same file list
        diff (list): Different file list
    """

    def __init__(self, file1=None, file2=None):
        self.f1 = file1
        self.f2 = file2
        self.same = {}
        self.diff = {}

    def compare(self):
        """ Compare the two file traces

        Compare the two files and find files that are the same
        and ones that are different.
        """

        for k in self.f1.file_names:
            if k in self.f2.file_names:
                self.same[k] = 1
            else:
                self.diff[k] = 1
        # now check files that are in f2 but not in f1
        for k in self.f2.file_names:
            if k not in self.f1.file_names:
                self.diff[k] = 2

if __name__ == '__main__':
    """ Manually parse files trace files and output differences

    Accepts two file names that are file traces to use.
    """

    parser1 = StraceParser(sys.argv[1], None, None, 0)
    parser2 = StraceParser(sys.argv[2], None, None, 0)
    parser1.parse_all()
    parser2.parse_all()
    result = FileActivityCompare(parser1.files, parser2.files)
    result.compare()
    print("Diff")
    print(sorted(result.diff.keys()))
    print("Same")
    print(sorted(result.same.keys()))
