#!/bin/bash

export PYTHONPATH="$PYTHONPATH:`pwd`"

if [ -n "${PYTHONBIN}" ];
    then echo "Using python: ${PYTHONBIN}";
    else echo "Python set to 3";
        export PYTHONBIN=python3
fi

$PYTHONBIN tests/StraceParseOpenTest.py -v
$PYTHONBIN tests/StraceParseCloseTest.py -v 
$PYTHONBIN tests/StraceParseReadWriteTest.py -v 
$PYTHONBIN tests/StraceParseUnknownTest.py -v
$PYTHONBIN tests/DatabaseCreateTest.py -v
$PYTHONBIN tests/SqliteCreateTest.py -v
$PYTHONBIN tests/FileCompareTest.py -v
