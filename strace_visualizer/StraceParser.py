# Flashlight debugging tools
# Copyright (C) 2016 Marta Rybczynska
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import re
import pprint
from Database import Database
from Database import FileProperties
from Database import FileEvent

try:
    from builtins import filter
except ImportError:
    pass


class FileTracer:
    """ File tracer

    This class stores aggregate information on the files in the traced
    process and outputs all events to a backend database.

    If you're tracing a real execution, you should use the initialize()
    method before adding other events.

    Arguments:
        db (Database): database backend to use

    Attributes:
        db (Database): database
        _dict (dict): Current file state directory
        opens (dict): Event id for each opened file
        history (dict): Previously opened files
        file_names (dict): Stores all file names
        pp (PrettyPrinter): pretty printer for debugging
    """

    def __init__(self, db):
        self.db = db
        self._dict = {}
        self.opens = {}
        self.opens[0] = 0
        self.opens[1] = 0
        self.opens[2] = 0
        self.history = {}
        self.file_names = {}
        self.pp = pprint.PrettyPrinter(indent=4)

    def add(self, id, val):
        """ Create an add-event

        Create a new add event, pass it to the database and
        update the open file list and filenames.

        Arguments:
            id (int): Descriptor id
            val (str): Open file attributes
        """

        self._dict[id] = val
        event_id = self.db.add_event(id, val)
        self.opens[id] = event_id
        if val['file_type'] == 'file':
            self.file_names[val['file_name']] = val

    def remove(self, id):
        """ Create a close-event

        Create a new close event, pass it to the database and
        update the open file list and filenames. Add the file to
        history.

        Arguments:
            id (int): Descriptor id
            val (str): Open file attributes
        """
        event_id = self.db.remove_event(id)
        self.history[self.opens[id]] = {
            self._dict[id]['file_name'],
            self.opens[id], event_id, self._dict[id]['read_increment'],
            self._dict[id]['write_increment']}
        del self._dict[id]

    def get_all(self):
        """
        Get all existing files state
        """

        return self._dict

    def initialize(self):
        """ Initialize or re-initialize

        Initialize file properties, create descriptors 0, 1 and 2 and add them
        """
        stdin = FileProperties()
        stdin['file_type'] = 'stdin'
        stdout = FileProperties()
        stdout['file_type'] = 'stdout'
        stderr = FileProperties()
        stderr['file_type'] = 'stderr'
        self.add(0, stdin)
        self.add(1, stdout)
        self.add(2, stderr)

    def do_event(self, event):
        """ Add a new event

        Add the new event to the database and update internal state.

        Arguments:
            event(event): Event details
        """
        if (event['read_increment']):
            self._dict[event['fd']]['read_increment'] += \
                event['read_increment']

        if (event['write_increment']):
            self._dict[event['fd']]['write_increment'] += \
                event['write_increment']

        self.db.do_event(event)

    def print_state(self):
        """ Pretty-print state

        Pretty print the history and current state.
        """

        self.pp.pprint(parser.files.history)
        self.pp.pprint(parser.files._dict)


class FileErrors(dict):
    """ File error store

    This class stores file related errors.
    """

    def __init__(self):
        self['name'] = {}


class StraceParser:
    """ Parser for the strace output

    This class parses the strace output and generates events of different
    operations

    Arguments:
        infile (str): Input file name
        outfile (str): Log output file name
        dbfile (str): File name of the database to create
        verbose_level (int): Verbosity level (0=no debug, 2=much debug)
    Attributes:
        inf (file): Input file
        outf (file): Output file
        db (Database): Backend database
        verbose (int): Verbosity level
        files (FileTracer): file tracer class
        errors (FileErrors): file error class
    """

    def __init__(
            self, infile=None, outfile=None, dbfile=None, verbose_level=0):
        if infile is not None:
            self.inf = open(infile, 'r')
        else:
            self.inf = None

        if outfile is not None:
            self.outf = open(outfile, 'w')
        else:
            self.outf = None

        self.db = Database()
        if dbfile is not None:
            self.db.add_backend('file', dbfile)

        self.verbose = verbose_level
        self.files = FileTracer(self.db)
        self.files.initialize()
        self.errors = FileErrors()

    def __del__(self):
        if self.inf:
            self.inf.close()
        if self.outf:
            self.outf.close()
        self.files = {}
        self.errors = {}
        self.verbose = 0

    def lookup_method(self, command):
        """ Find the right method for the syscall

        Find a do_SYSCALLNAME type of a method to handle the syscall.

        Arguments:
            command (str): Syscall name
        """
        return getattr(self, 'do_' + command.upper(), None)

    def add_open_operation(self, name, result, args):
        """ Add a generic open operation

        Add an event for file create operations like open() or socket().

        Arguments:
            name (str): File name passed to syscall
            result (int): Syscall result
            args (dict): Syscall attributes
        """
        if self.verbose:
            print(name + ' ' + str(args) + str(result))
        # parse the result to see if it worked
        res = result.split(' ')[0].strip()
        fd = int(res)
        if (fd >= 0):
            self.files.add(fd, args)
            if self.verbose > 2:
                print(self.files)
        else:
            self.errors[args['file_name']] = {
                args['attributes'], args['mode'],
                args['location'], args['domain'], args['type'],
                args['protocol'], result}
            if self.verbose > 2:
                print(self.errors)

    def do_OPEN(self, name, argument, result, line_no):
        """ Handle open syscall

        Handle open() syscalls like:
            open("/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3"

        Attributes:
            name (str): File name
            argument (str): Unparsed part of the arguments
            result (int): Syscall result
            line_no (int): Line number in the input file
        """

        # Splitting on: , <space> ( )
        parsed_args = list(filter(None, re.split("[, \(\)]+", argument)))
        args = FileProperties()
        # if we have the 3rd optional argument
        if len(parsed_args) > 2:
            args['mode'] = parsed_args[2]
        args['file_name'] = parsed_args[0].strip('"')
        args['file_type'] = 'file'
        args['attributes'] = parsed_args[1]
        self.add_open_operation('OPEN', result, args)

    def do_OPENAT(self, name, argument, result, line_no):
        """ Handle openat syscall

        Handle open() syscalls like:
            openat(AT_FDCWD, "/usr/lib/kde4/plugins/styles",
                O_RDONLY|O_NONBLOCK|O_DIRECTORY|O_CLOEXEC) = 7

        Attributes:
            name (str): File name
            argument (str): Unparsed part of the arguments
            result (int): Syscall result
            line_no (int): Line number in the input file
        """

        # Splitting on: , <space> ( )
        parsed_args = list(filter(None, re.split("[, \(\)]+", argument)))
        args = FileProperties()
        # if we have the 4th optional argument
        if len(parsed_args) > 3:
            args['mode'] = parsed_args[3]
        args['file_name'] = parsed_args[1].strip('"')
        args['file_type'] = 'file'
        args['attributes'] = parsed_args[2]
        args['location'] = parsed_args[0]
        self.add_open_operation('OPENAT', result, args)

    def do_SOCKET(self, name, argument, result, line_no):
        """ Handle socket syscall

        Handle socket() syscalls like:
            socket(PF_LOCAL, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, 0) = 9

        Attributes:
            name (str): File name
            argument (str): Unparsed part of the arguments
            result (int): Syscall result
            line_no (int): Line number in the input file
        """

        parsed_args = list(filter(None, re.split("[, \(\)]+", argument)))
        args = FileProperties()
        args['file_type'] = 'socket'
        args['domain'] = parsed_args[0]
        args['type'] = parsed_args[1]
        args['protocol'] = parsed_args[2]
        self.add_open_operation('SOCKET', result, args)

    def do_ACCEPT(self, name, argument, result, line_no):
        """ Handle accept syscall

        Handle accept() syscall like:
            accept(17, {sa_family=AF_LOCAL, NULL}, [2]) = 19

        Attributes:
            name (str): File name
            argument (str): Unparsed part of the arguments
            result (int): Syscall result
            line_no (int): Line number in the input file
        """

        parsed_args = list(filter(None, re.split("[, {}\(\)]+", argument)))
        args = FileProperties()
        args['file_type'] = 'socket'
        args['parent_socket'] = parsed_args[0]
        args['sockaddr'] = parsed_args[1] + ","+parsed_args[2]
        args['socklen'] = parsed_args[3]
        self.add_open_operation('ACCEPT', result, args)

    def do_PIPE2(self, name, argument, result, line_no):
        """ Handle pipe2 syscall

        Handle pipe2() syscalls like:
            pipe2([4, 5], O_NONBLOCK|O_CLOEXEC)     = 0

        Attributes:
            name (str): File name
            argument (str): Unparsed part of the arguments
            result (int): Syscall result
            line_no (int): Line number in the input file
        """
        if self.verbose:
            print('PIPE2 ' + name + argument + result)
        res = result.split(' ')[0].strip()
        fd = int(res)
        if (fd >= 0):
            # We have two file descriptors in args
            # Splitting on: , <space> - ! ? : [ ]
            parsed_args = list(filter(
                None, re.split("[, \-!?:\[\]]+", argument)))

            first = int(parsed_args[0])
            second = int(parsed_args[1])
            args = FileProperties()
            args['file_type'] = 'pipe'
            args['attributes'] = parsed_args[2]
            self.files.add(int(first), args)
            self.files.add(int(second), args)
            if self.verbose > 2:
                print(self.files)
        else:
            self.errors[args[1]] = {args, result}
            if self.verbose > 2:
                print(self.errors)
            return

    def do_EVENTFD2(self, name, argument, result, line_no):
        """ Handle eventfd2 syscall

        Handle eventfd() syscall like:
            eventfd2(0, O_NONBLOCK|O_CLOEXEC)       = 3

        Attributes:
            name (str): File name
            argument (str): Unparsed part of the arguments
            result (int): Syscall result
            line_no (int): Line number in the input file
        """

        parsed_args = list(filter(None, re.split("[, \(\)]+", argument)))
        args = FileProperties()
        args['file_type'] = 'eventfd'
        args['initval'] = parsed_args[0]
        args['flags'] = parsed_args[1]
        self.add_open_operation('EVENTFD', result, args)

    def do_INOTIFY_INIT(self, name, argument, result, line_no):
        """ Handle inotify_init

        Handle inotify_init() syscall like:
            inotify_init()                          = 10

        Attributes:
            name (str): File name
            argument (str): Unparsed part of the arguments
            result (int): Syscall result
            line_no (int): Line number in the input file
        """

        parsed_args = list(filter(None, re.split("[, \(\)]+", argument)))
        args = FileProperties()
        args['file_type'] = 'inotify'
        self.add_open_operation('INOTIFY', result, args)

    def do_INOTIFY_INIT1(self, name, argument, result, line_no):
        """ Handle inotify_init1

        Handle inotify_init1() syscall like:
            inotify_init1(O_CLOEXEC)                = 11

        Attributes:
            name (str): File name
            argument (str): Unparsed part of the arguments
            result (int): Syscall result
            line_no (int): Line number in the input file
        """

        parsed_args = list(filter(None, re.split("[, \(\)]+", argument)))
        args = FileProperties()
        args['file_type'] = 'inotify'
        args['flags'] = parsed_args[0]
        self.add_open_operation('INOTIFY', result, args)

    def do_CLOSE(self, name, argument, result, line_no):
        """ Handle close syscall

        Handle close() syscall like:
            close(1)                                = 0

        Attributes:
            name (str): File name
            argument (str): Unparsed part of the arguments
            result (int): Syscall result
            line_no (int): Line number in the input file
        """

        if self.verbose:
            print(str(line_no) + ' CLOSE ' + argument)
        fd = int(argument)
        self.files.remove(fd)
        if self.verbose > 2:
            print(self.files)
        return

    def do_READ(self, name, args, result, line_no):
        """ Handle read syscall

        Handle read() syscall like:
            read(3, "\177ELF\2\1\1\3\0\0\0\0\0\0\0\0\3\0\34"..., 832) = 832

        Attributes:
            name (str): File name
            argument (str): Unparsed part of the arguments
            result (int): Syscall result
            line_no (int): Line number in the input file
        """

        if self.verbose:
            print(str(line_no) + ' READ ' + args + ' result = ', result)
        # print self.files._dict
        event = FileEvent()
        # split on first and last comma
        event['fd'] = int(args.split(',', 1)[0].strip())
        event['type'] = 'read'
        event['length'] = int(args.rsplit(',', 1)[1].strip())
        event['result'] = int(result.split(' ')[0])
        if (int(result.split(' ')[0]) > 0):
            event['read_increment'] = int(result)
        self.files.do_event(event)

    def do_WRITE(self, name, args, result, line_no):
        """ Handle write syscall

        Handle write() syscall like:
            write(3, "\x01\x00\x00\x00\x00\x00\x00\x00", 8) = 8

        Attributes:
            name (str): File name
            argument (str): Unparsed part of the arguments
            result (int): Syscall result
            line_no (int): Line number in the input file
        """

        if self.verbose:
            print(' WRITE ' + args + ' result = ', result)
        # print self.files._dict
        event = FileEvent()
        # split on first and last comma
        event['fd'] = int(args.split(',', 1)[0].strip())
        event['type'] = 'write'
        event['length'] = int(args.rsplit(',', 1)[1].strip())
        event['result'] = int(result.split(' ')[0])
        if (int(result.split(' ')[0]) > 0):
            event['write_increment'] = int(result)
        else:
            print('WRITE ERROR line '+line_no)
        self.files.do_event(event)

    def do_UNKNOWN(self, name, args, result, line_no):
        """ Handle unknown syscall

        Handle unknown syscall by putting it in the output log file
        (if present).
        """

        if self.verbose:
            if self.outf:
                self.outf.write(
                    'skipping syscall ' + name + ' ' + args + ' ' + result +
                    '\n')
        # raise NotImplementedError, 'received unknown syscall ' + name

    def parse_one(self, entry, line_no=0):
        """ Parse one line of the strace file

        Parse one line in the format syscall(args) = result
        Split on equal sign and first opening ( and last closing ).
        """

        syscall = entry.rsplit('=', 1)[0].strip()
        syscall_name = syscall.split('(', 1)[0].strip()
        syscall_args = syscall.split('(', 1)[1].strip(')')
        result = entry.rsplit('=', 1)[1].strip()

        method = self.lookup_method(syscall_name) or self.do_UNKNOWN
        method(syscall_name, syscall_args, result, line_no)

    def parse_all(self):
        """
        Parse strace file line by line
        """

        line_no = 0
        for entry in self.inf.readlines():
            if ('+++' not in entry):
                if self.verbose > 2:
                    print(entry)
                self.parse_one(entry, line_no)
            line_no += 1


if __name__ == '__main__':
    parser = StraceParser(sys.argv[1], sys.argv[2], sys.argv[3], 0)
    parser.parse_all()
