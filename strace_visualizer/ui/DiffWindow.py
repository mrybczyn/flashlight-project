# Flashlight debugging tools
# Copyright (C) 2016 Marta Rybczynska
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *


class DiffWindow(QWidget):
    """ File diff window

    This class shows a window that shows file operations
    differnce between two executions.


    Arguments:
        none

    Attributes:
        none
    """

    def __init__(self, _data_same=None, _data_diff=None):
        QWidget.__init__(self)

        self.treeView = QTreeView()

        self.model = QStandardItemModel()
        self.addFileItems(self.model, _data_same, _data_diff)
        self.treeView.setModel(self.model)

        self.model.setHorizontalHeaderLabels([self.tr("Files")])

        layout = QVBoxLayout()
        layout.addWidget(self.treeView)
        self.setLayout(layout)

    def addItems(self, parent, elements):
        """ Add items to the view

        This function adds items at the given place of the model.

        Arguments:
            parent(QModel): the element to add to
            elements(str): the elements to add
        """
        for text, children in elements:
            item = QStandardItem(text)
            parent.appendRow(item)
            if children:
                self.addItems(item, children)

    def find_in_tuple(self, li, element):
        """ Find a position in a tuple list

        This function searches element in a pair of
        (string, list) lists. If the string part matches,
        it returns its index. If no match is found it returns -1

        Arguments:
            li(list): tuple list to search in
            element(str): string to search

        Returns:
            index if found, -1 otherwise
        """
        index = 0
        for i in li:
            if (i[0] == element):
                return index
            index += 1
        return -1

    def addFileItems(self, parent, elements_same, elements_diff):
        """ Add items in a form of a file path

        This function adds items that look like file paths
        to the model.

        Arguments:
            parent(QModel): the element to add to
            elements_same(str): the elements to add
            elements_diff(str): the elements to add
        """
        items = []
        for item in elements_same:
            item = item.lstrip('/')
            path = item.split('/')
            path_len = len(path)
            level = 0
            items_copy = items
            for d in path:
                if (self.find_in_tuple(items_copy, d) == -1):
                    items_copy.append((d, "black", []))
                index = self.find_in_tuple(items_copy, d)
                items_copy = items_copy[index][2]
                level += 1
        for item in elements_diff:
            item = item.lstrip('/')
            path = item.split('/')
            path_len = len(path)
            level = 0
            items_copy = items
            for d in path:
                if (self.find_in_tuple(items_copy, d) == -1):
                    items_copy.append((d, "red", []))
                    items_copy.sort()
                index = self.find_in_tuple(items_copy, d)
                items_copy = items_copy[index][2]
                level += 1
        for item in items:
            self.addSingleFileItem(parent, item)

    def addSingleFileItem(self, parent, li):
        """ Add one file path level

        This function adds one file item that looks like file
        paths to the model. It then follows to its leafs

        Arguments:
            parent(QModel): the element to add to
            li(tuple): the element to add in a form of
                  (string, color, [new list of items])
        """
        # create item from the string part
        item = QStandardItem(li[0])
        item.setForeground(QBrush(QColor(li[1])))
        parent.appendRow(item)
        # add subitems if we have something on the list
        if (len(li[2]) > 0):
            for i in li[2]:
                self.addSingleFileItem(item, i)

if __name__ == "__main__":
    my_data = [
        "/dev/tty",
        "/dev/urandom",
        "/etc/fonts/conf.d/10-scale-bitmap-fonts.conf"]
    my_data2 = ["/tmp/blah"]
    app = QApplication(sys.argv)
    window = DiffWindow(my_data, my_data2)
    window.show()
    sys.exit(app.exec_())
